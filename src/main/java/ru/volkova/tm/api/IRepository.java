package ru.volkova.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.entity.AbstractEntity;

import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    Optional<E> add(@NotNull E entity);

    void remove(@NotNull E entity);

}
