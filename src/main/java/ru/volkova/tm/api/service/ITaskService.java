package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.entity.Task;

import java.util.Optional;

public interface ITaskService extends IOwnerService<Task> {

    @NotNull
    Optional<Task> add(
            @NotNull String userId,
            @Nullable String name,
            @Nullable String description
    );

}
