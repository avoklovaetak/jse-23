package ru.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show application version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("2.0.0");
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

}
