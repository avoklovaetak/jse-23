package ru.volkova.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.enumerated.Role;

@NoArgsConstructor
public abstract class AbstractCommand {
    
    @Nullable
    protected ServiceLocator serviceLocator;
    
    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();
    
    public abstract void execute();

    @Nullable
    public abstract String name();

    @Nullable
    public Role[] roles() {
        return null;
    }

    public AbstractCommand(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
