package ru.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @NotNull
    @Override
    public Optional<E> add(@NotNull final E entity) {
        entities.add(entity);
        return Optional.of(entity);
    }

    @NotNull
    protected final Predicate<E> predicateById(@NotNull String id) {
        return e -> id.equals(e.getId());
    }

    @Override
    public void remove(@NotNull final E entity) {
        entities.remove(entity);
    }

}
