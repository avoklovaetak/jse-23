package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.IAuthService;
import ru.volkova.tm.api.service.IUserService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.empty.EmptyLoginException;
import ru.volkova.tm.exception.empty.EmptyPasswordException;
import ru.volkova.tm.exception.entity.UserNotFoundException;
import ru.volkova.tm.util.HashUtil;

import java.util.Optional;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void checkRoles(@Nullable Role... roles) {
        if (roles == null || roles.length == 0) return;
        @NotNull final Optional<User> user = getUser();
        if (!user.isPresent()) throw new AccessDeniedException();
        @Nullable final Boolean locked = user.get().getLocked();
        if (!locked) throw new AccessDeniedException();
        final Role role = user.get().getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) {
            if (role.equals(item)) return;
        }
        throw new AccessDeniedException();
    }

    @NotNull
    @Override
    public Optional<User> getUser() {
        @NotNull final String userId = getUserId();
        return userService.findById(userId);
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    public void login(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final Optional<User> user = userService.findByLogin(login);
        if (!user.isPresent()) throw new UserNotFoundException();
        final String hash = HashUtil.salt(password);
        if (hash == null || hash.isEmpty()) return;
        if (!hash.equals(user.get().getPasswordHash())) throw new AccessDeniedException();
        userId = user.get().getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    public void registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        userService.create(login, password, email);
    }

}
